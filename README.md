# Carbon Fields Extended Multiselect Extension

This extension adds a Multiselect field to the Carbon Fields library, with the added ability to retrieve either the label, value, or key/value array of the selected options.

## Installation

To install the extension, you can use the following command:

```shell
composer require mywebsos/carbon-fields-extended-multiselect
```

## Usage

To use the Extended Multiselect field in your Carbon Fields container, you can use the following code:

```php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'post_meta', __( 'Extended Multiselect Field' ) )
    ->add_fields( array(
        Field::make( 'extended_multiselect', 'extended_multiselect_field', __( 'Select options' ) )
            ->set_options( array(
                'option_1' => 'Option 1',
                'option_2' => 'Option 2',
                'option_3' => 'Option 3',
            ) )
            ->return_format( 'array' ) // label, array or value ( default )
    ) );
```

You can retrieve the selected options using the following methods:

```php
// Retrieve the label of the selected options
$value = carbon_get_post_meta( get_the_ID(), $field_name );
```

## Note

Make sure to include the use statement of Carbon_Fields\Field at the top of your file.
