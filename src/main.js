/**
 * External dependencies.
 */
import MultiselectField from "../vendor/htmlburger/carbon-fields/packages/core/fields/multiselect";

class ExtendedMultiselectField extends MultiselectField {}

export default ExtendedMultiselectField;
