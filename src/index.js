/**
 * External dependencies.
 */
import { registerFieldType } from "@carbon-fields/core";

/**
 * Internal dependencies.
 */
import "./style.scss";
import ExtendedMultiselectField from "./main";

registerFieldType("extended_multiselect", ExtendedMultiselectField);
