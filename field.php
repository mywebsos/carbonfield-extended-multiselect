<?php

use Carbon_Fields\Carbon_Fields;
use Carbon_Field_Extended_Multiselect\Extended_Multiselect_Field;

define( 'Carbon_Field_Extended_Multiselect\\DIR', __DIR__ );

Carbon_Fields::extend( Extended_Multiselect_Field::class, function( $container ) {
	return new Extended_Multiselect_Field(
		$container['arguments']['type'],
		$container['arguments']['name'],
		$container['arguments']['label']
	);
} );
