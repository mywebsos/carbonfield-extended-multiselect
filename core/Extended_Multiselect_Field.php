<?php
declare(strict_types=1);

namespace Carbon_Field_Extended_Multiselect;

use Carbon_Fields\Field\Multiselect_Field;

/**
 * Class Extended_Multiselect_Field
 *
 * Custom field type for Carbon Fields library that extends the built-in Multiselect Field.
 * It allows for additional options and functionality.
 *
 * @package Carbon_Field_Extended_Multiselect
 */
class Extended_Multiselect_Field extends Multiselect_Field {

	/**
	 * The format in which the field value should be returned.
	 *
	 * @var string
	 */
	protected string $return_format = 'value';

	/**
	 * Create a field from a certain type with the specified label.
	 *
	 * @param string $type  Field type.
	 * @param string $name  Field name.
	 * @param string $label Field label.
	 */
	public function __construct(string $type, string $name, string $label) {
		parent::__construct( $type, $name, $label );
	}

	/**
	 * Prepare the field type for use.
	 * Called once per field type when activated.
	 *
	 * @static
	 * @access public
	 *
	 * @return void
	 */
	public static function field_type_activated(): void {
		$dir = \Carbon_Field_Extended_Multiselect\DIR . '/languages/';
		$locale = get_locale();
		$path = $dir . $locale . '.mo';
		load_textdomain( 'carbon-field-extended-multiselect', $path );
	}

	/**
	 * Enqueue scripts and styles in admin.
	 * Called once per field type.
	 *
	 * @static
	 * @access public
	 *
	 * @return void
	 */
	public static function admin_enqueue_scripts(): void {
		$root_uri = \Carbon_Fields\Carbon_Fields::directory_to_url( \Carbon_Field_Extended_Multiselect\DIR );

		// Enqueue field styles.
		wp_enqueue_style( 'carbon-field-extended-multiselect', $root_uri . '/build/bundle.css' );

		// Enqueue field scripts.
		wp_enqueue_script( 'carbon-field-extended-multiselect', $root_uri . '/build/bundle.js', array( 'carbon-fields-core' ) );
	}

	/**
	 * Get the formatted value of the field.
	 * 
	 * @return array
	 */
	public function get_formatted_value(): array {
		$options = $this->get_options();
		$value = $this->get_value();
		$values = array();
		
		foreach( $value as $v ) {
			$key = $this->get_values_from_options( array( $v ) );
			if( 'value' === $this->return_format ) {
				$values[] = $options[ $key[0] ];
			} elseif( 'array' === $this->return_format ) {
				$values[$key[0]] = $options[$key[0]];
			} else {
				$values[] = $key[0];
			}
		}

		return $values;
	}

	/**
	 * Set the format in which the field value should be returned.
	 *
	 * @param string $return_format The format in which the field value should be returned.
	 *
	 * @return $this
	 */
	public function return_format(string $return_format): self {
		$this->return_format = $return_format;
		return $this;
	}
	
	/**
	 * Returns the field value as a JSON-encoded string.
	 * Modify the field properties that are passed to the React component which represents the field.
	 *
	 * @param bool $load Should the value be loaded from the database or use the current value.
	 *
	 * @return array
	 */
	public function to_json($load): array {
		$field_data = parent::to_json($load);
		$options = $this->parse_options($this->get_options(), true);
		$value = $this->get_values_from_options($value);		
		
		$field_data = array_merge($field_data, array(
			'options' => $options,
			'value' => $value,
		));

		return $field_data;
	}
}
